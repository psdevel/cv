import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ErrorComponent } from 'src/app/components/error/error.component';

@NgModule({
  imports: [RouterModule.forChild([
    {path: 'error', children: [
      {path: '', component: ErrorComponent},
      {path: '**', redirectTo: '', pathMatch: 'full'},
    ]},
    {path: '404', component: ErrorComponent}
  ])],
  exports: [RouterModule]
})
export class ErrorRouteModule {}
