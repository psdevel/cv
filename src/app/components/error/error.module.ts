import { NgModule } from '@angular/core';
import { ErrorComponent } from 'src/app/components/error/error.component';
import { ErrorRouteModule } from 'src/app/components/error/error.route';

@NgModule({
  declarations: [
    ErrorComponent,
  ],
  exports: [],
  imports: [
    ErrorRouteModule,
  ],
  providers: []
})
export class ErrorModule {}
