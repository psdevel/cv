import { NgModule } from '@angular/core';
import { CommonModule } from 'src/app/components/common/common.module';
import { ErrorModule } from 'src/app/components/error/error.module';
import { CvModule } from 'src/app/components/cv/cv.module';

@NgModule({
  imports: [
    CommonModule,
    ErrorModule,
    CvModule,
  ],
  providers: [],
  exports: []
})
export class ComponentsModule {}
