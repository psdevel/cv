import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-components-common',
  templateUrl: './common.component.html',
  styleUrls: ['./common.component.min.css']
})
export class CommonComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
