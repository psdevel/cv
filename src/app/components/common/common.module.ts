import { NgModule } from '@angular/core';
import { CommonComponent } from 'src/app/components/common/common.component';
import { CommonRouteModule } from 'src/app/components/common/common.route';

@NgModule({
  declarations: [
    CommonComponent,
  ],
  exports: [],
  imports: [
    CommonRouteModule,
  ],
  providers: []
})
export class CommonModule {}
