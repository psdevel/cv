import { CommonComponent } from 'src/app/components/common/common.component';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [RouterModule.forChild([
    {path: 'common', children: [
      {path: '', component: CommonComponent},
      {path: '**', redirectTo: '', pathMatch: 'full'},
    ]}
  ])],
  exports: [RouterModule]
})
export class CommonRouteModule {}
