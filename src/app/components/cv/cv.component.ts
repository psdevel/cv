import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/shared/entities/user-entity/user.entity';
import { Cv } from 'src/app/shared/entities/cv-entity/cv.entity';

@Component({
  selector: 'app-cv',
  templateUrl: './cv.component.html',
  styleUrls: ['./cv.component.min.css', './cv-print.component.min.css']
})
export class CvComponent implements OnInit {
  user: User;
  cv: Cv;

  constructor() {
    // Mock
    this.cv = new Cv();
    this.cv.position = 'Frontend Developer';
    this.cv.workLog = [
      {
        company: 'Aimtec, a.s.',
        dateFrom: 'June 2019', dateTo: 'Present', text: '' +
          '<p>Migrate product frontend from JavaServer Pages to Anglar 8. Using:' +
          '<ul>' +
          '<li>Angular 8</li>' +
          '<li>Design app structure</li>' +
          '<li>Design css structure</li>' +
          '</ul>' +
          '</p>'
      },
      {
        company: 'Itify, s.r.o.',
        dateFrom: 'June 2017', dateTo: 'May 2019', text: '' +
          '<p>Private custom projects and integrations. Working with agile software development approach. ' +
          'Collaborating in Jira and BitBucket. Using:' +
          '<ul>' +
          '<li>Angular 4, 7</li>' +
          '<li>SCSS</li>' +
          '<li>PrimeNG and Material Design Components</li>' +
          '<li>Typescript</li>' +
          '<li>Javascript</li>' +
          '<li>Wireframing</li>' +
          '<li>UX Testing</li>' +
          '<li>Manage styles with Gulp Toolkit</li>' +
          '<li>Git</li>' +
          '</ul>' +
          '</p>'
      },
      {
        company: 'Kerio Technologies',
        dateFrom: 'May 2015', dateTo: 'May 2017', text: '' +
          '<p>Support for internal sales system. Refactoring old code, developing new features. Using:' +
          '<ul>' +
          '<li>PHP</li>' +
          '<li>jQuery</li>' +
          '<li>MySQL</li>' +
          '<li>CSS</li>' +
          '<li>Apache</li>' +
          '<li>MS Excel VBA</li>' +
          '</ul>' +
          '</p>'
      }
    ];
    this.cv.educationLog = [
      {
        school: 'West Bohemia University, Pilsen',
        dateFrom: '2011', dateTo: '2014', text: '' +
          '<p>Study for two specializations: \'Business Economics and Management\' and \'Information Management\' ' +
          'both dropped. Learned basics of:' +
          '<ul>' +
          '<li>Economy</li>' +
          '<li>Company Management</li>' +
          '<li>Project Management</li>' +
          '<li>Marketing</li>' +
          '<li>Business communication</li>' +
          '</ul>' +
          '</p>'
      }];
    this.cv.skills = [
      {skill: 'Photoshop', percentage: 90},
      {skill: 'Angular 4, 7', percentage: 85},
      {skill: 'PHP 7', percentage: 80},
      {skill: 'SCSS', percentage: 80},
      {skill: 'UX Design', percentage: 60}
    ];
    this.cv.languages = [
      {language: 'Czech', level: 'native'},
      {language: 'Russian', level: 'native'},
      {language: 'English', level: 'B2 level'},
      {language: 'Spanish', level: 'A2 level'},
      {language: 'Ukrainian', level: 'A2 level'}
    ];
    this.cv.hobbies = ['Social dancing', 'Mountain biking', 'IOT', 'Photography'];


    this.user = new User();
    this.user.firstName = 'Pavlo';
    this.user.secondName = 'Sherin';
    this.user.phone = '+(420) 736 437 665';
    this.user.email = 'sherin.pavel@gmail.com';
    this.user.web = {href: 'https://www.sherin.cloud', display: 'www.sherin.cloud'};
    this.user.address = 'Pilsen';
    this.user.about = '<p>I am a highly competent frontend developer with a passion in designing websites. I have strong ' +
      'technical skills as well as excellent interpersonal skills, enabling me to interact with a wide range of clients. I am ' +
      'eager to be challenged in order to grow and further improve my skills. I feel that my life mission is using my technical ' +
      'know-how to help other people.</p>';
    this.user.image = '../assets/img/IMG_0030.jpg';
  }

  ngOnInit() {
  }

}

