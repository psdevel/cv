import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CvComponent } from 'src/app/components/cv/cv.component';

@NgModule({
  imports: [RouterModule.forChild([
    {path: 'cv', children: [
      {path: '', component: CvComponent},
      {path: '**', redirectTo: '', pathMatch: 'full'},
    ]}
  ])],
  exports: [RouterModule]
})
export class CvRouteModule {}
