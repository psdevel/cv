import { NgModule } from '@angular/core';
import { CvComponent } from 'src/app/components/cv/cv.component';
import { CvRouteModule } from 'src/app/components/cv/cv.route';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    CvComponent,
  ],
  exports: [],
  imports: [
    CommonModule,
    CvRouteModule,
  ],
  providers: []
})
export class CvModule {}
