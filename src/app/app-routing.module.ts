import { NgModule } from '@angular/core';
import { Routes, RouterModule, Router } from '@angular/router';
import { CvComponent } from 'src/app/components/cv/cv.component';

const appRoutes: Routes = [
  {path: '', component: CvComponent},
  {path: '**', redirectTo: '', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
  constructor(private router: Router) {
    this.router.errorHandler = (error: any) => {
      this.router.navigate(['404']); // or redirect to default route
    };
  }
}
