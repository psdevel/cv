export class Cv {
  id: number;
  position: string;
  workLog: {company: string, dateFrom: string, dateTo: string, text: string}[];
  educationLog: {school: string, dateFrom: string, dateTo: string, text: string}[];
  skills: {skill: string, percentage: number}[];
  languages: {language: string, level: string}[];
  hobbies: string[];
}
