export class User {
  id: number;
  titleBefore: string;
  firstName: string;
  secondName: string;
  titleAfter: string;
  phone: string;
  email: string;
  web: {display: string, href: string};
  address: string;
  about: string;
  skill: {skill: string, percentage: number}[];
  image: string;
}
