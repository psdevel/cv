// //////////////////////////////////
// REQUIRES
// //////////////////////////////////
const
  gulp = require('gulp'),
  tap = require('gulp-tap'),
  log = require('fancy-log'),
  env = require('gulp-env'),
  rename = require('gulp-rename'),
  uglify_css = require('gulp-clean-css'),
  sass = require('gulp-sass'),
  sourcemaps = require('gulp-sourcemaps'),
  lazypipe = require('lazypipe'),
  clean = require('gulp-clean');

sass.compiler = require('node-sass');

// //////////////////////////////////
// VARIABLES
// //////////////////////////////////
env({
  file: '.env.json'
});

const
  gulpAppFolder = 'app',
  gulpSrcFolder = gulpAppFolder + '/src',
  gulpDistFolder = gulpAppFolder + '/dist';

const createMinCss = lazypipe()
.pipe(sourcemaps.init)
.pipe(sass)
.pipe(sourcemaps.init, {loadMaps: true})
.pipe(uglify_css, {compatibility: 'ie8'})
.pipe(sourcemaps.write)
.pipe(rename, {suffix: '.min'});

// //////////////////////////////////
// ENVIRONMENT VARIABLES
// //////////////////////////////////
let
  appDistFolder = process.env.appDistFolder;

// //////////////////////////////////
// TASKS
// //////////////////////////////////
gulp.task('_clean:assets', () => {
  return gulp.src(gulpDistFolder + '/assets', {allowEmpty: true})
  .pipe(clean());
});

gulp.task('_clean:components', () => {
  return gulp.src(gulpDistFolder + '/app', {allowEmpty: true})
  .pipe(clean());
});

gulp.task('_build:components', () => {
  return gulp.src(gulpSrcFolder + '/app/**/*.scss')
  .pipe(createMinCss())
  .pipe(gulp.dest(gulpDistFolder + '/app'));
});

gulp.task('_build:assets', () => {
  return gulp.src(gulpSrcFolder + '/assets/styles.scss')
  .pipe(createMinCss())
  .pipe(gulp.dest(gulpDistFolder + '/assets/css'));
});

gulp.task('_export', () => {
  return gulp.src(gulpDistFolder + '/**/*.*')
  .pipe(tap((file) => {
    const filePathToDestination = file.path.replace(
      file.cwd + '/' + gulpDistFolder, '');
    const finalFilePath = appDistFolder + filePathToDestination.replace(
      file.basename, '');
    gulp.src(file.path)
    .pipe(gulp.dest(finalFilePath));
  }));
});

// //////////////////////////////////
// COMPOUND TASKS
// //////////////////////////////////
gulp.task('build:assets', gulp.series(
  ['_clean:assets', '_build:assets']
));
gulp.task('build:components', gulp.series(
  ['_clean:components', '_build:components']
));
gulp.task('export', gulp.series(
  ['build:assets', 'build:components', '_export']
));

// //////////////////////////////////
// WATCH TASKS
// //////////////////////////////////
// gulp.task('watch', () => {
//     gulp.watch(gulpSrcFolder + 'scss/**/*.scss', gulp.series(['process-scss']));
//     gulp.watch(gulpAppFolder + 'index.html').on('change', browser_sync.reload);
//     browser_sync.init({
//         server: {baseDir: gulpAppFolder}
//     });
// });
gulp.task('watch', () => {
  gulp.watch(gulpSrcFolder, gulp.series(['export']));
});

// //////////////////////////////////
// DEFAULT TASKS
// //////////////////////////////////
gulp.task('help', function (done) {
  console.log(
    'gulp or gulp default or gulp help  : Run this help');
  console.log(
    'gulp build:assets                  : Clears gulp dist folder and builds asset styles.');
  console.log(
    'gulp build:components              : Clears gulp dist folder and builds styles for angular project components.');
  console.log(
    'gulp clear:assets                  : Clears gulp dist folder from assets.');
  console.log(
    'gulp clear:components              : Clears gulp dist folder from components.');
  console.log(
    'gulp export                       : Runs internally build:assets and build:components then copy dist files into angular project ('
    + appDistFolder + '/). Src folder should copy angular project structure.');
  console.log(
    'gulp watch                         : Watches gulp src folder for changes then run export.');
  done();
});

gulp.task('default', gulp.series(
  ['help']
));
